let contactData = {"name": "Jan", "surname": "Kowalski", "email": "jan.kow@mail.com", "twitter": "blabla@twitter"};

let project1 = {"projectName": "project 1", "projectDescription": "Description of project 1"};
let project2 = {"projectName": "project 2", "projectDescription": "Description of project 2"};


let aboutMe = {
    "baseInformation": contactData,
    "projects": [project1, project2]
};


function populateProjectData() {
    populateName();
    populateSurname();
    populateMail();
    populateTwitter();
    populateProjects();
}


function populateName() {
    populate(aboutMe.baseInformation.name, "myName");
}

function populateSurname() {
    populate(aboutMe.baseInformation.surname, "surname");
}

function populateMail() {
    populate(aboutMe.baseInformation.email, "email")
}

function populateTwitter() {
    populate(aboutMe.baseInformation.twitter, "twitter")
}


function populateProjects() {
    let projectsTextTable =[];
    for (let i = 0; i < aboutMe.projects.length; i++) {
        projectsTextTable.push("Name of project: " + aboutMe.projects[i].projectName);
        projectsTextTable.push("Description : " + aboutMe.projects[i].projectDescription);
    }

    populate(projectsTextTable.join("\n"), "projects");
}


function populate(data, targetId) {
    let targetElement = document.getElementById(targetId);

    targetElement.innerText = data;
}
populateProjectData();

